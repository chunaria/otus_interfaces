﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  class Program
  {
    static async Task Main(string[] args)
    {
      var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), "a5cf9da55cb835d0a633a7825b3aa8b5");
      var transactionRepository = new InMemoryTransactionRepository();
      var transactionParser = new TransactionParser();
      var budgetApp = new BudjetApplication(transactionRepository, transactionParser, currencyConverter);

      bool continueCycle = true;
      while (continueCycle)
      {
        continueCycle = MainCycle(budgetApp);
      }
    }

    public static bool MainCycle(BudjetApplication budjet)
    {
      Console.Clear();
      UI.PrintMenu();
      char key = Console.ReadKey(true).KeyChar;
      ICommand command = CommandFactory.CreateCommand(UI.GetCommand(key));
      return command.GetInputAndExecute(budjet);
    }
  }
}
