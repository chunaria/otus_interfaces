﻿using System;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class Income : ITransaction
  {
    public static int MinParamCount => 4;
    public ICurrencyAmount Amount { get; }
    public DateTimeOffset Date { get; }
    public string Source { get; }

    public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
    {
      Amount = amount;
      Date = date;
      Source = source;
    }

    public override string ToString() => $"Зачисление {Amount} от {Source} поступило {Date}";

    public string ToParsedString()
    {
      return $"Зачисление {Amount.Amount} {Amount.CurrencyCode} {Source} {Date:s}";
    }

    public static Income Parse(string[] splits)
    {
      if (splits.Length < Income.MinParamCount)
      {
        UI.ReportError("Вводная строка для транзакции зачисления должна содержать как мининум 4 параметра ");
        return null;
      }

      DateTimeOffset date;
      if (splits.Length == Income.MinParamCount  || !DateTimeOffset.TryParse(splits[4], out date))
      {
        date = DateTimeOffset.Now;
      }

      CurrencyAmount currencyAmount = new CurrencyAmount(splits[2].ToUpper(), decimal.Parse(splits[1]));
      return new Income(currencyAmount, date, splits[3]);
    }

  }
}
