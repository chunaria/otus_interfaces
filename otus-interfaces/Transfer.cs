﻿using System;
using System.Linq;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class Transfer : ITransaction
  {
    private const int MinParamCount = 5;
    public ICurrencyAmount Amount { get; }
    public DateTimeOffset Date { get; }

    public string Destination { get; }
    public string Message { get; }

    public Transfer(ICurrencyAmount amount, DateTimeOffset date, string destination, string message)
    {
      Amount = amount;
      Date = date;
      Message = message;
      Destination = destination;
    }

    public override string ToString() => $"Перевод {Amount} на имя {Destination} с сообщением {Message} совершен {Date}";

    public string ToParsedString()
    {
      return $"Перевод {Amount.Amount} {Amount.CurrencyCode} {Destination} {Message} {Date:s}";
    }

    public static Transfer Parse(string[] splits)
    {
      if (splits.Length < MinParamCount)
      {
        UI.ReportError("Вводная строка для транзакции траты должна содержать как мининум 5 параметров ");
        return null;
      }

      string message = "";
      DateTimeOffset date;
      if (splits.Length == MinParamCount || !DateTimeOffset.TryParse(splits[splits.Length - 1], out date))
      {
        date = DateTimeOffset.Now;
        message = string.Join(" ", splits.Skip(4).Take(splits.Length - 4));
      }

      if (string.IsNullOrWhiteSpace(message))
        message = string.Join(" ", splits.Skip(3));
      CurrencyAmount currencyAmount = new CurrencyAmount(splits[2].ToUpper(), decimal.Parse(splits[1]));
      return new Transfer(currencyAmount, date, splits[3], message);
    }
  }
}
