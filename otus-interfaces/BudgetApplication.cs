﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class BudjetApplication : IBudgetApplication
  {
    private readonly ICurrencyConverter _currencyConverter;
    private readonly ITransactionParser _transactionParser;

    private ITransactionRepository _transactionRepository;

    public BudjetApplication(ITransactionRepository transactionRepository, ITransactionParser transactionParser, ICurrencyConverter currencyConverter)
    {
      _currencyConverter = currencyConverter;
      _transactionRepository = transactionRepository;
      _transactionParser = transactionParser;
    }

    public void AddTransaction(string input)
    {
      var transaction = _transactionParser.Parse(input);
      if (transaction != null)
      {
        _transactionRepository.AddTransaction(transaction);
      }
    }

    public void OutputTransactions()
    {
      foreach (var transaction in _transactionRepository.GetTransactions())
      {
        Console.WriteLine(transaction);
      }
      Console.ReadLine();
    }

    public void OutputBalanceInCurrency(string currencyCode)
    {
      try
      {
        var totalCurrencyAmount = new CurrencyAmount(currencyCode, 0);
        var amounts = _transactionRepository.GetTransactions()
            .Select(t => t.Amount)
            .Select(a => a.CurrencyCode != currencyCode ? _currencyConverter.ConvertCurrency(a, currencyCode) : a)
            .Where(a => a != null)
            .ToArray();

        var totalBalanceAmount = amounts.Aggregate(totalCurrencyAmount, (t, a) => t += a);

        Console.WriteLine($"Баланс: {totalBalanceAmount}");
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        Console.WriteLine($"Баланс неизвестен");
      }

      Console.ReadLine();
    }

    public void SetNewRepository(ITransactionRepository newRepository)
    {
      if (newRepository == null)
      {
        UI.ReportError("Репозиторий не создан");
        return;
      }
      foreach (var transaction in _transactionRepository.GetTransactions())
      {
        newRepository.AddTransaction(transaction);
      }

      _transactionRepository = newRepository;
      Console.WriteLine("Репозиторий создан");
      Console.ReadLine();
    }
  }
}
