﻿using System;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class TransactionParser : ITransactionParser
  {
    public ITransaction Parse(string input)
    {
      var splits = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
      if (splits.Length < 4)
      {
        UI.ReportError("Вводная строка для транзакции должна содержать как мининум 4 параметра разделенных пробелом");
        return null;
      }

      var typeCode = splits[0];

        switch (typeCode)
        {
          case "Трата":
            return Expense.Parse(splits);
          case "Зачисление":
            return Income.Parse(splits);
          case "Перевод":
            return Transfer.Parse(splits);
          default:
          {
            UI.ReportError($"Тип транзакции {typeCode} не поддерживается");
            return null;
          }
        }
    }
  }
}
