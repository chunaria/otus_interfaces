﻿using System;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
    public class Expense : ITransaction
    {
        public Expense(ICurrencyAmount amount, DateTimeOffset date, string destination, string category)
        {
            Amount = amount;            
            Date = date;
            Category = category;
            Destination = destination;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Category { get; }
        public string Destination { get; }
        public static int MinParamCount => 5;

        public override string ToString() => $"Трата {Amount} в {Destination} по категории {Category} произошла {Date}";
        public string ToParsedString()
        {
          return $"Трата {Amount.Amount} {Amount.CurrencyCode} {Destination} {Category} {Date:s}";
        }

        public static Expense Parse(string[] splits)
        {
          if (splits.Length < Expense.MinParamCount)
          {
            UI.ReportError("Вводная строка для транзакции траты должна содержать как мининум 5 параметров ");
            return null;
          }

          DateTimeOffset date;
          if (splits.Length == Expense.MinParamCount  || !DateTimeOffset.TryParse(splits[5], out date))
          {
            date = DateTimeOffset.Now;
          }

          CurrencyAmount currencyAmount = new CurrencyAmount(splits[2].ToUpper(), decimal.Parse(splits[1]));
          return new Expense(currencyAmount, date, splits[3], splits[4]);
        }
    }
}
