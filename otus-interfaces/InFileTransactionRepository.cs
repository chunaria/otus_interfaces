﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class InFileTransactionRepository : ITransactionRepository
  {
    private readonly string _fileName;

    public InFileTransactionRepository(string input)
    {
      bool rw = input.EndsWith("rw", true, CultureInfo.InvariantCulture);

      _fileName = input.TrimEnd(" rw".ToCharArray()).TrimEnd(" RW".ToCharArray());

      try
      {
        string directoryPath = Path.GetDirectoryName(_fileName);
        if (!Directory.Exists(directoryPath))
        {
          Directory.CreateDirectory(directoryPath);
        }

        if (rw || !File.Exists(_fileName))
        {
          FileStream file = File.Create(_fileName);
          file.Dispose();
        }
        else
        {
          GetTransactions();
        }
      }
      catch (Exception e)
      {
        UI.ReportError($"Невозможно подключить такой репозиторий: {e.Message}");
      }
    }

    public void AddTransaction(ITransaction transaction)
    {
      if (transaction == null)
      {
        return;
      }

      File.AppendAllLines(_fileName, new[] { transaction.ToParsedString() });
    }

    public ITransaction[] GetTransactions()
    {
      TransactionParser parser = new TransactionParser();
      List<ITransaction> transactions = new List<ITransaction>();
      foreach (var line in File.ReadAllLines(_fileName))
      {
        ITransaction transaction = parser.Parse(line);
        if (transaction != null)
          transactions.Add(transaction);
      }
      return transactions.ToArray();
    }
  }
}
