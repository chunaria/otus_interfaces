﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus_interfaces.UserInput
{

  public enum commandCode
  {
    newTransaction,
    printTransactions,
    getBalance,
    newFileRepo,
    exit,
    unknown
  }

  public static class UI
  {
    public static void ReportError(string message)
    {
      Console.WriteLine(message);
    }
    public static void PrintMenu()
    {
      Console.WriteLine(
        "Нажмите \nn  - добавить транзакцию\np  - посмотреть список транзакций\nb  - узнать балланс в заданой валюте\nf - подключить новый файловый репозиторий для хранения транзакций\ne  -  выйти");
    }

    public static commandCode GetCommand(char key )
    {
        switch (key)
        {
          case ('n'):
            return commandCode.newTransaction;
          case ('p'):
            return commandCode.printTransactions;
          case ('b'):
            return commandCode.getBalance;
          case ('f'):
            return commandCode.newFileRepo;
          case ('e'):
            return commandCode.exit;
          default:
            return commandCode.unknown;
        }
    }

  }
}
