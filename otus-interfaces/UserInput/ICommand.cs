﻿using System;
using System.Collections.Generic;
using System.Text;

namespace otus_interfaces.UserInput
{
  public interface ICommand
  {
    bool GetInputAndExecute(BudjetApplication budjet);
  }

  public class Command: ICommand
  {
    private readonly string _hint;
    private Func<string, BudjetApplication, bool> _executeCommand;

    public Command(string hint, Func<string, BudjetApplication, bool> executeCommand)
    {
      _hint = hint;
      _executeCommand = executeCommand;
    }

    public bool GetInputAndExecute( BudjetApplication budjet)
    {
      string input = "";
      if (!string.IsNullOrEmpty(_hint))
      {
        Console.WriteLine(_hint);
        input = Console.ReadLine();
      }
      return _executeCommand(input, budjet);
    }
  }

}
