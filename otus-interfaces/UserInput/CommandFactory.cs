﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace otus_interfaces.UserInput
{
  public static class CommandFactory
  {
    private const string newTransactionHint = "Введите данные о транзакции в формате [тип] [сумма] [валюта] [назначение/источник] [категория/сообщение]";
    private const string newFileRepoHint = "Введите путь к файлу нового репозитория для хранения транзакций. Добавьте в конец строки через пробел rw, если хотите чтобы файл был перезаписан."; 
    private const string getBalanceHint = "Введите код валюты"; 
    private const string unknownCommandHint = "Такой команды не существует"; 
    public static ICommand CreateCommand(commandCode code)
    {
      switch (code)
      {
        case commandCode.newTransaction:
          return new Command(newTransactionHint,(input, budjet) =>
          {
            budjet.AddTransaction(input);
            return  true;
          });
        case commandCode.newFileRepo:
          return new Command(newFileRepoHint,(input, budjet) =>
          {
            InFileTransactionRepository repo = new InFileTransactionRepository(input);
            budjet.SetNewRepository(repo) ;
            return  true;
          });
        case commandCode.getBalance:
          return new Command(getBalanceHint,(input, budjet) =>
          {
            budjet.OutputBalanceInCurrency(input) ;
            return true;
          });
        case commandCode.printTransactions:
          return new Command("",(input, budjet) =>
          {
            budjet.OutputTransactions() ;
            return true;
          });
        case commandCode.exit:
          return new Command("",(input, budjet) => false);
        default:
          return new Command(unknownCommandHint, (input, budjet)=>true);
      }
    }
  }
}
