﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using otus_interfaces.UserInput;

namespace otus_interfaces
{
  public class ExchangeRatesApiConverter: ICurrencyConverter
  {
    private readonly HttpClient _httpClient;
    private readonly IMemoryCache _memoryCache;
    private readonly string _apiKey;
    private const string cacheKey = "rates";
    public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
    {
      _memoryCache = memoryCache;
      _httpClient = httpClient;
      _apiKey = apiKey;
    }
    public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
    {
      Dictionary<string, double> rates;
      if (!_memoryCache.TryGetValue(cacheKey, out rates))
      {
        rates = GetExchangeRatesAsync().Result.Rates;
        _memoryCache.Set(cacheKey, rates, TimeSpan.FromDays(1));
      }
      string oldCode = amount.CurrencyCode;
      double oldRates, newRates;
      if(!rates.TryGetValue(oldCode, out oldRates)||!rates.TryGetValue(currencyCode, out newRates))
      {
        UI.ReportError("Неизвестный код валюты");
        return null;
      }
      return new CurrencyAmount(currencyCode, amount.Amount*(decimal)(newRates/oldRates));
    }

    private async Task<ExchangeRatesApiResponse> GetExchangeRatesAsync()
    {
      var response = await _httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}");
      response = response.EnsureSuccessStatusCode();
      var json = await response.Content.ReadAsStringAsync();
      return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(json);
    }

    public class ExchangeRatesApiResponse
    {
      [JsonProperty("success")]
      public bool Success { get; set; }

      [JsonProperty("timestamp")]
      public long Timestamp { get; set; }

      [JsonProperty("base")]
      public string Base { get; set; }

      [JsonProperty("date")]
      public DateTimeOffset Date { get; set; }

      [JsonProperty("rates")]
      public Dictionary<string, double> Rates { get; set; }
    }
  }
}
