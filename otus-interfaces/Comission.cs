﻿using System;

namespace otus_interfaces
{
    public class Comission : ITransaction // декоратор
    {
        public ITransaction OriginalTransaction { get; }
        public ICurrencyAmount Amount { get; }
        public string ToParsedString()
        {
          throw new NotImplementedException();//чтобы полностью реализовать функционал как у остальных транзакций нужно вводить поле id транзакции
        }

        public DateTimeOffset Date { get; }

        public override string ToString() => $"Комиссия в размере {Amount} за транзакцию: {OriginalTransaction}";
    }
}
